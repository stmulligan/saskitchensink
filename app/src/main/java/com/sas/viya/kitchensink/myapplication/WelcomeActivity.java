package com.sas.viya.kitchensink.myapplication;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import Utils.Const;
import cropimage.CropImageActivity;

public class WelcomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
    {
     String versionText = Const.EMPTY_STRING;
    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        updateVersion();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
            {
            @Override
            public void onClick(View view)
                {
                Snackbar.make(view, versionText, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                }
            });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        }

    @Override
    public void onBackPressed()
        {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            {
            drawer.closeDrawer(GravityCompat.START);
            }
        else
            {
            super.onBackPressed();
            }
        }

    private void updateVersion()
        {
        PackageInfo pInfo = null;
        try
            {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionText  = "v" + pInfo.versionName;
            }
        catch (PackageManager.NameNotFoundException e)
            {
            e.printStackTrace();
            versionText = " v0.0";
            }

        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        menu.getItem(0).setIcon(R.drawable.ic_wrench);
        return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings)
            {
            Intent intent = new Intent(this, SettingsActivity.class);
            //EditText editText = (EditText) findViewById(R.id.editText);
            //String message = editText.getText().toString();
            //intent.putExtra("", message);
            startActivity(intent);
            return true;
            }

        return super.onOptionsItemSelected(item);
        }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
        {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        System.out.println("Welcome: " + item.toString());

        if (id == R.id.nav_fraud)
            {

            }
        else if (id == R.id.nav_product)
            {

            }
        else if (id == R.id.nav_anomaly)
            {
            Intent intent = new Intent(this, CropImageActivity.class);
            startActivity(intent);
            }
        else if (id == R.id.nav_ml)
            {

            }
        else if (id == R.id.nav_image)
            {
            Intent intent = new Intent(this, UploaderActivity.class);
            startActivity(intent);
            }
        else if (id == R.id.nav_share)
            {

            }
        else if (id == R.id.nav_send)
            {

            }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
        }
    }
