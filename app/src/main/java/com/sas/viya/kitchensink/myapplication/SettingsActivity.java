package com.sas.viya.kitchensink.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity
    {

    EditText serverText, emailText, passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        bindControls();
        }

    private void hideSoftKeyboard()
        {
        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(passwordText.getWindowToken(), 0);
        }

    private void bindControls()
        {
        serverText = findViewById(R.id.server_text);
        emailText = findViewById(R.id.email_text);
        passwordText = findViewById(R.id.password_text);
        passwordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus)
            {
            hideSoftKeyboard();
            }
        }
        });
        }

    public void doConnect(View v) // connect button clicked
    {
    finish();
    }
    }