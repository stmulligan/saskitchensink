package com.sas.viya.kitchensink.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

import Utils.Const;
import Utils.ImageUtil;
import cropimage.CropImageActivity;

public class UploaderActivity extends AppCompatActivity
    {
    private Uri selectedImageUri = null;
    private String TAG = getClass().getSimpleName();
    private ImageView imageview;
    private String imagepath;
    private File sourceFile;
    private int totalSize = 0;
    private LinearLayout uploader_area;
    private LinearLayout progress_area;
    public ProgressBar u_progress;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private final int bitmapsize = 100;
    private Bitmap bitmapToUpload = null;
    private Bitmap bitmapOriginal = null;
    private String bitmapToUploadPath = Const.EMPTY_STRING;
    private Button select_button, upload_button;
    private String uploadFileName = "UploadFile.jpg";
    private TextView result_text;
    private Toolbar toolbar;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uploader_activity);
        ctx = new WeakReference<Context>(this).get();
        bindControls();
        getPermissions();
        setCallbacks();
        }

    private void setCallbacks()
        {
        select_button.setOnClickListener(new View.OnClickListener()
            {
            @Override
            public void onClick(View view)
                {
                upload_button.setText(getString(R.string.classify));
                result_text.setText(Const.EMPTY_STRING);
                Intent intent = new Intent();
                intent.setType("image/*"); // intent.setType("video/*"); to select videos to upload
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1000);
                }
            });

        upload_button.setOnClickListener(new View.OnClickListener()  // upload file to REST endpoint
            {
            @Override
            public void onClick(View view)
                {
                if (selectedImageUri.toString() != null)
                    {
                    bitmapToUpload = Bitmap.createScaledBitmap(bitmapOriginal, bitmapsize, bitmapsize, false);
                    bitmapToUploadPath = saveToInternalStorage(bitmapToUpload);
                    // new UploadTask().execute();
                    new Thread(new Runnable()
                        {
                        @Override
                        public void run()
                            {
                            uploadFile(bitmapToUploadPath);
                            }
                        }).start();
                    }
                else
                    {
                    Toast.makeText(getApplicationContext(), "Please select an image to upload.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    private void getPermissions()
        {
        Boolean hasPermission = (ContextCompat.checkSelfPermission(UploaderActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission)
            {
            ActivityCompat.requestPermissions(UploaderActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
            }
        else
            {
            }
        }

    private void bindControls()
        {
        uploader_area = (LinearLayout) findViewById(R.id.uploader_area);
        progress_area = (LinearLayout) findViewById(R.id.progress_area);
        select_button = (Button) findViewById(R.id.button_selectpic);
        select_button.setText(getString(R.string.select_image));
        upload_button = (Button) findViewById(R.id.button_upload);
        u_progress = (ProgressBar) findViewById(R.id.u_progress);
        imageview = (ImageView) findViewById(R.id.imageview);
        result_text = (TextView) findViewById(R.id.result_txt);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null)
            {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
        {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
            {
            case REQUEST_WRITE_STORAGE:
            {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                //reload my activity with permission granted or use the features what required the permission
                }
            else
                {
                Toast.makeText(UploaderActivity.this, "You must give access to storage.", Toast.LENGTH_LONG).show();
                }
            }
            }
        }

    @Override
    public boolean onSupportNavigateUp()
        {
        onBackPressed();
        return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        if (item.getItemId() == android.R.id.home)
            {
            finish();
            }
        return super.onOptionsItemSelected(item);
        }

    private String saveToInternalStorage(Bitmap bitmapImage)
        {
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());
        File file = wrapper.getDir("Images", MODE_PRIVATE);
        file = new File(file, this.uploadFileName);
        try
            {
            OutputStream stream = null;
            stream = new FileOutputStream(file);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
            }
        catch (IOException e) // Catch the exception
            {
            e.printStackTrace();
            }

        Uri savedImageURI = Uri.parse(file.getAbsolutePath());
        Log.i(TAG, "Saved bitmap: " + savedImageURI.toString());
        return savedImageURI.toString();
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent returnedExtras)
        {
        if (requestCode == 1000 && resultCode == RESULT_OK)  // user picked image from gallery
            {
            selectedImageUri = returnedExtras.getData();
            Log.i(TAG, "Gallery uri: " + selectedImageUri.toString());
            try
                {
                bitmapOriginal = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                if (bitmapOriginal.getWidth() != bitmapOriginal.getHeight())  // image selected is not square. need to crop it.
                    {
                    Intent intent = new Intent(ctx, CropImageActivity.class);
                    intent.putExtra(CropImageActivity.INPUT_IMAGE_PATH, selectedImageUri.toString()); //
                    intent.putExtra(CropImageActivity.OUTPUT_IMAGE_PATH, Const.SQUARE_IMAGE_FILENAME); //
                    intent.putExtra(CropImageActivity.SCALE, true);// allow CropImage activity to rescale image
                    intent.putExtra(CropImageActivity.ASPECT_X, 6);
                    intent.putExtra(CropImageActivity.ASPECT_Y, 6);
                    intent.putExtra(CropImageActivity.OUTPUT_X, 100);
                    intent.putExtra(CropImageActivity.OUTPUT_Y, 100);
                    startActivityForResult(intent, 5800);
                    }
                else
                    {
                    imageview.setImageBitmap(bitmapOriginal);
                    }
                }
            catch (IOException e)
                {
                e.printStackTrace();
                }
                        }
        else if (requestCode == 5800 && resultCode == RESULT_OK)  // square image returned from CropImageActivity. Load the square image...
            {
            String bitmapFilename = ImageUtil.getSavePath() + Const.SQUARE_IMAGE_FILENAME;
            Bitmap fromFile = ImageUtil.loadFromFile(bitmapFilename);
            Log.i(TAG, "SquareImage created. Loading: " + bitmapFilename);
                Bundle bundle = returnedExtras.getExtras();
                Bitmap bitmap = bundle.getParcelable(CropImageActivity.RETURN_DATA_AS_BITMAP);
                imageview.setImageBitmap(fromFile);
            }
        }

    public int uploadFile(String sourceFileUri)
        {
        File file = new File(sourceFileUri);
        StringRequestListener stringRequestListener = new StringRequestListener()
            {
            @Override
            public void onResponse(String response)
                {
                String[][] StatesAndCapitals =
                        {{"Alabama", "Montgomery"},
                                {"Alaska", "Juneau"},
                                {"Kansas", "Topeka"},
                                {"Kentucky", "Frankfort"},
                                {"Louisiana", "Baton Rouge"},
                                {"Maine", "Augusta"}};
                JSONObject obj = null;
                String probline = Const.EMPTY_STRING;
                try
                    {
                    String resultLines = Const.EMPTY_STRING;
                    obj = new JSONObject(response);
                    JSONArray probs_arr = obj.getJSONArray("probs");
                    String true_label = obj.getJSONObject("true_label").getString("0");
                    result_text.setText(true_label);

                    for (int i = 0; i < probs_arr.length(); i++)
                        {
                        JSONObject j = probs_arr.getJSONObject(i);
                        String probItem = j.keys().next();
                        String probNumber = j.getString(probItem);
                        probline += probItem.replace("P__label_", Const.EMPTY_STRING) + " : " + String.format("%.4f", Double.parseDouble(probNumber)) + Const.NEWLINE;
                        }
                    }
                catch (JSONException e)
                    {
                    e.printStackTrace();
                    }
                result_text.setText(probline);
                }

            @Override
            public void onError(ANError anError)
                {
                Log.i(TAG, "HTTP Error: " + anError.toString());
                upload_button.setText("Server: " + anError.toString());
                }
            };
        //AndroidNetworking.upload(Const.UPLOAD_URL)
        AndroidNetworking.upload(Const.GTP_URL)
                .addMultipartFile("Image", file)
                .addMultipartParameter("key", "value")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener()
                    {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes)
                        {
                        // do anything with progress
                        }
                    })
                .getAsString(stringRequestListener);
        return 0;
        }

    public class UploadTask extends AsyncTask<String, String, String>
        {
        @Override
        protected void onPreExecute()
            {
            super.onPreExecute();
            //u_progress.setProgress(0);
            //uploader_area.setVisibility(View.GONE); // Making the uploader area screen invisible
            //progress_area.setVisibility(View.VISIBLE); // Showing the stylish material progressbar
            sourceFile = new File(bitmapToUploadPath);
            totalSize = (int) sourceFile.length();
            Log.i(TAG, "Uploading " + sourceFile.getAbsolutePath() + ": " + totalSize + " bytes...");
            }

        @Override
        protected void onProgressUpdate(String... progress)
            {
            super.onProgressUpdate();
            //Log.d("PROG", progress[0]);
            //u_progress.setProgress(Integer.parseInt(progress[0])); //Updating progress
            }

        @Override
        protected String doInBackground(String... args)
            {
            HttpURLConnection.setFollowRedirects(true);
            HttpURLConnection connection = null;
            File uploadFile = new File(bitmapToUploadPath);

            try
                {
                connection = (HttpURLConnection) new URL(Const.UPLOAD_URL).openConnection();
                connection.setRequestMethod("POST");
                String boundary = "---------------------------boundary";
                String tail = "\r\n--" + boundary + "--\r\n";
                connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                connection.setDoOutput(true);
                String metadataPart = "--" + boundary + "\r\n" + "Content-Disposition: form-data; name=\"metadata\"\r\n\r\n" + "" + "\r\n";
                String fileHeader1 = "--" + boundary + "\r\n"
                        + "Content-Disposition: form-data; name=\"" + uploadFileName + "\"; filename=\"" + uploadFileName + "\"\r\n"
                        + "Content-Type: application/octet-stream\r\n"
                        + "Content-Transfer-Encoding: binary\r\n";
                long fileLength = uploadFile.length() + tail.length();
                String fileHeader2 = "Content-length: " + fileLength + "\r\n";
                String fileHeader = fileHeader1 + fileHeader2 + "\r\n";
                String stringData = metadataPart + fileHeader;
                Log.i(TAG, "Header: " + fileHeader);
                long requestLength = stringData.length() + fileLength;
                connection.setRequestProperty("Content-length", "" + requestLength);
                connection.setFixedLengthStreamingMode((int) requestLength);
                connection.connect();

                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                out.writeBytes(stringData);
                out.flush();

                int progress = 0;
                int bytesRead = 0;
                byte buf[] = new byte[1024];
                BufferedInputStream bufInput = new BufferedInputStream(new FileInputStream(sourceFile));
                while ((bytesRead = bufInput.read(buf)) != -1)
                    {
                    // write output
                    out.write(buf, 0, bytesRead);
                    out.flush();
                    progress += bytesRead; // Here progress is total uploaded bytes

                    //publishProgress("" + (int) ((progress * 100) / totalSize)); // sending progress percent to publishProgress
                    }
                // Write closing boundary and close stream
                out.writeBytes(tail);
                out.flush();
                out.close();

                // Get server response
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                StringBuilder builder = new StringBuilder();
                while ((line = reader.readLine()) != null)
                    {
                    builder.append(line);
                    }

                }
            catch (Exception e)
                {
                // Exception
                }
            finally
                {
                if (connection != null)
                    {
                    connection.disconnect();
                    }
                }
            return null;
            }

        @Override
        protected void onPostExecute(String result)
            {
            Log.e(TAG, "Response from server: " + result);
            upload_button.setText("Response: " + result);
            super.onPostExecute(result);
            }
        }
    }